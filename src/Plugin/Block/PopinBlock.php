<?php

namespace Drupal\popin\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'PopinBlock' block.
 *
 * @Block(
 *  id = "popin_block",
 *  admin_label = @Translation("Popin block"),
 * )
 */
class PopinBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PopinBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('popin.popinconfig')->get();
    $build = [
      '#cache' => [
        'tags' => ['popin'],
        'max-age' => 3600,
      ],
    ];
    if (!isset($config['enabled']) || $config['enabled'] !== 1) {
      return $build;
    }
    $now = new DrupalDateTime();
    if (isset($config['datestart'])) {
      $dateStart = DrupalDateTime::createFromTimestamp($config['datestart']);
      if ($dateStart > $now) {
        return $build;
      }
    }
    if (isset($config['dateend'])) {
      $dateEnd = DrupalDateTime::createFromTimestamp($config['dateend']);
      if ($dateEnd < $now) {
        return $build;
      }
    }

    $image_style = $this->entityTypeManager->getStorage('image_style')
      ->load('popin');

    if (isset($image_style) && isset($config['image'][0]) && is_numeric($config['image'][0])) {
      $image = $this->entityTypeManager->getStorage('file')
        ->load($config['image'][0]);

      if (isset($image)) {
        $config['image'] = $image_style->buildUrl($image->getFileUri());
      }
    }
    $config['id'] = 'popin-' . substr(sha1($config['titre'] . $config['sous_titre']), 0, 5);

    $config['description'] = [
      '#type' => 'processed_text',
      '#text' => $config['description']['value'],
      '#format' => $config['description']['format'],
    ];

    $build['popin_block']['#theme'] = 'popin_block';
    $build['popin_block']['#config'] = $config;
    $build['#attached']['library'][] = 'popin/popin';

    return $build;
  }

}
