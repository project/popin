<?php

namespace Drupal\popin\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring the Popin module settings.
 *
 * This form allows administrators to set various options for the pop-up
 * window, such as display timing, content, and appearance settings.
 */
class PopinConfigForm extends ConfigFormBase {

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  private $fileStorage;

  /**
   * Constructs a new PopinConfigForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileUsageInterface $fileUsage, AccountProxyInterface $accountProxy, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->fileUsage = $fileUsage;
    $this->currentUser = $accountProxy;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new PopinConfigForm($container->get('config.factory'), $container->get('file.usage'), $container->get('current_user'), $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'popin.popinconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'popin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('popin.popinconfig');

    $form['basic'] = [
      '#type' => 'details',
      '#title' => $this->t('Popin display configuration'),
      '#open' => TRUE,
    ];

    $form['basic']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Popin enabled ?'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['basic']['cookie_ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('Cookie TTL (days)'),
      '#description' => $this->t('Number of days between two display of the popin for a same user'),
      '#min' => 0,
      '#default_value' => $config->get('cookie_ttl') ? $config->get('cookie_ttl') : 7,
    ];

    $form['basic']['datestart'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start date for popin display'),
      '#description' => $this->t('If left empty, the popin will be displayed without limit'),
      '#default_value' => $config->get('datestart') ? DrupalDateTime::createFromTimestamp($config->get('datestart')) : NULL,
    ];

    $form['basic']['dateend'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Ending date for popin display'),
      '#description' => $this->t('If left empty, the popin will be displayed without limit'),
      '#default_value' => $config->get('dateend') ? DrupalDateTime::createFromTimestamp($config->get('dateend')) : NULL,
    ];

    $form['content'] = [
      '#type' => 'details',
      '#title' => $this->t('Popin content'),
      '#open' => TRUE,
    ];

    $form['content']['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Popin image'),
      '#upload_location' => 'public://popin/',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#default_value' => $config->get('image'),
    ];

    $form['content']['titre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Popin title'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('titre'),
    ];

    $form['content']['sous_titre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subtitle'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('sous_titre'),
    ];

    $form['content']['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('description')['value'] ?? $config->get('description'),
      '#format' => $config->get('description')['format'] ?? NULL,
    ];

    $form['content']['texte_cta'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('texte_cta'),
    ];

    $form['content']['lien_cta'] = [
      '#type' => 'url',
      '#title' => $this->t('Link target (url)'),
      '#default_value' => $config->get('lien_cta'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $image = $form_state->getValue('image');
    if (isset($image[0])) {
      $file = $this->entityTypeManager->getStorage('file')->load($image[0]);
      if ($file && $file->isTemporary()) {
        $file->setPermanent();
        $this->fileUsage->add($file, 'popin', 'user', $this->currentUser->id());
        $file->save();
      }
    }
    $dateStart = $form_state->getValue('datestart');
    $dateEnd = $form_state->getValue('dateend');
    $this->config('popin.popinconfig')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('image', $form_state->getValue('image'))
      ->set('titre', $form_state->getValue('titre'))
      ->set('sous_titre', $form_state->getValue('sous_titre'))
      ->set('description', $form_state->getValue('description'))
      ->set('texte_cta', $form_state->getValue('texte_cta'))
      ->set('cookie_random', random_int(0, 10000))
      ->set('cookie_ttl', $form_state->getValue('cookie_ttl'))
      ->set('lien_cta', $form_state->getValue('lien_cta'))
      ->set('dateend', $form_state->getValue('dateend'))
      ->set('datestart', $dateStart ? $dateStart->format('U') : NULL)
      ->set('dateend', $dateEnd ? $dateEnd->format('U') : NULL)
      ->save();

    Cache::invalidateTags(['popin']);
  }

}
