(function($, cookies) {
  // Class creation
  const Popin = function(el) {
    this.init(el);
  };
  const P = Popin.prototype;

  // Parameters
  P.popinOffset = 60;

  // ======================================
  // @function init(el)
  // __ constructor
  // ======================================
  P.init = function(el) {
    this.$container = $(el);
    this.$cookieName = this.$container.data("popin-value");
    this.$cookieTTL = parseInt(this.$container.data("popin-ttl"), 10);
    this.$triggers = $(`[data-popin="${el.id}"]`);
    this.$actions = this.$container.find("[data-action]");
    if (parseInt(cookies.get(this.$cookieName), 10) !== 1) {
      this.$container.addClass("initialised");
      this.$container.addClass("active");
      // Event Listeners
      this.$triggers.on("click", this.open.bind(this));
      $(window).on("keydown", this.onKeyDown.bind(this));
      this.$actions.on("click", this.onClickAction.bind(this));
      // set Cookie
      cookies.set(this.$cookieName, 1, { expires: this.$cookieTTL, path: "/" });
    }
  };

  // ======================================
  // @function onKeyDown()
  //
  // ======================================
  P.onKeyDown = function(e) {
    // eslint-disable-next-line default-case
    switch (e.keyCode) {
      case 27: // echap
        this.close();
        break;
    }
  };

  // ======================================
  // @function onClickAction()
  //
  // ======================================
  P.onClickAction = function(e) {
    const action = e.currentTarget.attributes["data-action"].value;
    e.preventDefault();

    // eslint-disable-next-line default-case
    switch (action) {
      case "close":
        this.close();
        break;
    }
  };

  // ======================================
  // @function open()
  //
  // ======================================
  P.open = function(e) {
    this.$container.addClass("active");
  };

  // ======================================
  // @function close()
  //
  // ======================================
  P.close = function(e) {
    this.$container.removeClass("active");
  };

  // set the Class to global scope
  window.Popin = Popin;

  // Instanciation
  const nbCheckPopinMax = 5;
  let checkPopinInterval = 0;
  let nbCheckPopin = 0;

  const createPopins = function () {
    $(".popin").each(function() {
      if (!$(this).hasClass("initialised")) {
        // eslint-disable-next-line no-new
        new Popin(this);
      }
    });

    if (++nbCheckPopin >= nbCheckPopinMax) {
      clearInterval(checkPopinInterval);
    }
  };

  const checkPopinsJs = function () {
    checkPopinInterval = setInterval(createPopins, 1000);
  };
  createPopins();
  checkPopinsJs();
})(jQuery, window.Cookies);
